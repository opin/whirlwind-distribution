# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.2.1] - 2020-01-09
### Changed
- Removed Whirlwind Theme from composer.json.  Need to add it manually for it to not be overwritten on build.

## [1.1.13] - 2019-11-12
### Changed
- Updated composer to use the new theme url.

## [1.1.12] - 2019-11-12
### Changed
- Updated composer to use the new theme url.

## [1.1.11] - 2019-11-12
### Changed
- Updated composer to use the new theme url.

## [1.1.10] - 2019-11-12
### Changed
- Updated composer to use the new theme url.

## [1.1.8] - 2019-09-11
### Changed
- Updated theme name in yml info file.

## [1.1.5] - 2019-09-11
### Changed
- Updated theme name in profile file.

## [1.1.3] - 2019-09-11
### Changed
- Updated composer.json file.

## [1.1.0] - 2019-09-11
### Changed
- Updated composer.json file.

## [1.0.7] - 2019-09-11
### Changed
- Configuration files for the distribution.

## [1.0.5] - 2019-08-14
### Added
- Added admin_toolbar_tools to be enabled on setup.
- Added Whirlwind Theme to composer.json to pull in the latest version from Gitlab.
### Changed
- Removed Claro theme from composer.json.
- Removed unnecessary require-dev modules from composer.json.
- Updated whirlwind.profile to enable Whirlwind Theme by default.

## [1.0.4] - 2019-07-30
### Changed
- Updated Devel to 2.0 version.

## [1.0.3] - 2019-07-30
### Changed
- Updated composer.json file to remove Content Access module.  This module is only in alpha and causing issues.

## [1.0.2] - 2019-07-29
### Changed
- Updated composer.json file to remove Advanced Aggregation module.  Although this project is stable, the speed increase is minimal.  This project is also producing PHP errors in all code coverage tests.

## [1.0.0] - 2019-07-03
### Changed
- Upgraded to stable version.

## [0.1.0] - 2019-07-03
### Added
- Initial distribution setup and configuration.
- Added CHANGELOG.txt file.
- Updates README.md file.
- Include standard configuration.
